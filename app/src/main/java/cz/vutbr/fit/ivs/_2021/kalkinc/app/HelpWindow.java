/*
 * Copyright (c) 2021. Dítě Jindřich, Lebedenko Anastasiia, Čuhanič Jakub,
 *  Hradil Michal
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package cz.vutbr.fit.ivs._2021.kalkinc.app;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.LineBorder;

public class HelpWindow implements ActionListener {

    JFrame frame;
    JLabel label = new JLabel("Click on a button to see what it does");
    JButton[] Buttons = new JButton[13];
    JPanel buttons;
    JTextField textfield_1;
    JTextField textfield_2;
    JTextField textfield_3;
    JTextField textfield_4;
    //initializing buttons
    JButton delButton, negButton, logButton, powButton, rootButton;
    JButton clrButton, facButton;
    JButton par_leftButton, par_rightButton, ansButton, mButton, m_plusButton, m_minusButton;

    //font setting
    Font myFont = new Font("Roboto",Font.BOLD,15);

    HelpWindow() {

        frame = new JFrame("Help");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(615, 650);
        frame.getContentPane().setBackground(new Color (255, 255,255));
        frame.setLayout(null);
        frame.setResizable(false);
        frame.setVisible(true);

        //icon - continue
        ImageIcon image = new ImageIcon("app/src/image/Question_mark.png");
        frame.setIconImage(image.getImage());

        label.setBounds(25, 25, 400, 50);
        label.setFont(new Font("Roboto", Font.PLAIN, 20));
        frame.add(label);

        delButton = new JButton("DEL");
        clrButton = new JButton("CE");
        negButton = new JButton("\u00B1");
        logButton = new JButton("log");
        powButton = new JButton("x\u00B2");
        rootButton = new JButton("\u221A");
        facButton = new JButton("!");
        par_leftButton = new JButton("(");
        par_rightButton = new JButton(")");
        ansButton = new JButton ("Ans");
        mButton = new JButton ("M");
        m_plusButton = new JButton ("M+");
        m_minusButton = new JButton ("M-");
        //initializing arrays, setting action listeners, focusability
        Buttons[0] = par_leftButton;
        Buttons[1] = par_rightButton;
        Buttons[2] = mButton;
        Buttons[3] = m_minusButton;
        Buttons[4] = m_plusButton;
        Buttons[5] = clrButton;
        Buttons[6] = delButton;
        Buttons[7] = negButton;
        Buttons[8] = logButton;
        Buttons[9] = powButton;
        Buttons[10] = rootButton;
        Buttons[11] = facButton;
        Buttons[12] = ansButton;

        buttons = new JPanel();
        buttons.setBounds(25, 100, 490, 90);
        buttons.setLayout(new GridLayout(2,10,10,10));
        buttons.setBackground(new Color(255,255,255));

        for(int i =0;i<13;i++) {
            Buttons[i].addActionListener(this);
            Buttons[i].setFont(myFont);
            Buttons[i].setFocusable(false);
            Buttons[i].setBackground(new Color(183,255,203));
            Buttons[i].setBorder(new LineBorder(new Color(183,255,203)));
            buttons.add(Buttons[i]);
        }

        //panels' settings (what buttons go in there, size, position)

        textfield_1 = new JTextField();
        textfield_1.setBounds(25, 230, 550, 30);
        textfield_1.setFont(new Font("Roboto", Font.PLAIN, 20));
        textfield_1.setEditable(false);
        textfield_1.setBorder(new LineBorder((new Color(255, 255, 255)),0, false));
        textfield_1.setBackground(Color.WHITE);
        textfield_1.setVisible(true);

        textfield_2 = new JTextField();
        textfield_2.setBounds(25, 300, 550, 30);
        textfield_2.setFont(new Font("Roboto", Font.PLAIN, 20));
        textfield_2.setEditable(false);
        textfield_2.setBorder(new LineBorder((new Color(255, 255, 255)),0, false));
        textfield_2.setBackground(Color.WHITE);
        textfield_2.setVisible(true);

        textfield_3 = new JTextField();
        textfield_3.setBounds(25, 330, 550, 30);
        textfield_3.setFont(new Font("Roboto", Font.PLAIN, 20));
        textfield_3.setEditable(false);
        textfield_3.setBorder(new LineBorder((new Color(255, 255, 255)),0, false));
        textfield_3.setBackground(Color.WHITE);
        textfield_3.setVisible(true);

        textfield_4 = new JTextField();
        textfield_4.setBounds(25, 400, 550, 30);
        textfield_4.setFont(new Font("Roboto", Font.PLAIN, 20));
        textfield_4.setEditable(false);
        textfield_4.setBorder(new LineBorder((new Color(255, 255, 255)),0, false));
        textfield_4.setBackground(Color.WHITE);
        textfield_4.setVisible(true);

        //adding all the panels and separate buttons into a frame
        frame.add(buttons);
        frame.add(textfield_1);
        frame.add(textfield_2);
        frame.add(textfield_3);
        frame.add(textfield_4);
        frame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource()==par_rightButton){
            textfield_1.setText("    Adds a ')' to your expression.");
            textfield_2.setText("");
            textfield_3.setText("");
            textfield_4.setText("");
        }
        if (e.getSource()==par_leftButton){
            textfield_1.setText("    Adds a '(' to your expression.");
            textfield_2.setText("");
            textfield_3.setText("");
            textfield_4.setText("");
        }
        if (e.getSource()==mButton){
            textfield_1.setText("    Returns result, saved by M+.");
            textfield_2.setText("    If 2.0 is in memory ->");
            textfield_3.setText("    3 + M returns 5");
            textfield_4.setText("    M M+ multiplies a number in memory by 2.");
        }
        if (e.getSource()==m_minusButton){
            textfield_1.setText("    M M- sets a number in memory to 0.");
            textfield_2.setText("");
            textfield_3.setText("");
            textfield_4.setText("");
        }
        if (e.getSource()==m_plusButton){
            textfield_1.setText("    Saves result of last operation.");
            textfield_2.setText("    If 2.0 was returned by last operation ->");
            textfield_3.setText("    after pressing Ans M+ 2.0 is saved to memory.");
            textfield_4.setText("    M M+ multiplies a number in memory by 2.");
        }
        if (e.getSource()==clrButton){
            textfield_1.setText("    Deletes the whole expression.");
            textfield_2.setText("");
            textfield_3.setText("");
            textfield_4.setText("");
        }
        if (e.getSource()==delButton){
            textfield_1.setText("    Deletes the last character/number of expression.");
            textfield_2.setText("");
            textfield_3.setText("");
            textfield_4.setText("");
        }
        if (e.getSource()==negButton){
            textfield_1.setText("    Sets a negative sign ( from 5 to (-5) ).");
            textfield_2.setText("");
            textfield_3.setText("");
            textfield_4.setText("");
        }
        if (e.getSource()==logButton){
            textfield_1.setText("    Returns log of a number.");
            textfield_2.setText("    The input can be [base] log [number],");
            textfield_3.setText("    or log [number] with implicit base of 10.");
            textfield_4.setText("");
        }
        if (e.getSource()==powButton){
            textfield_1.setText("    If the input is x ^ y, returns x raised to the power of y. ");
            textfield_2.setText("");
            textfield_3.setText("");
            textfield_4.setText("");
        }
        if (e.getSource()==rootButton){
            textfield_1.setText("    If the input is x \u221A y, returns x's root of y.");
            textfield_2.setText("");
            textfield_3.setText("");
            textfield_4.setText("");
        }
        if (e.getSource()==facButton){
            textfield_1.setText("    Returns a factorial of a number.");
            textfield_2.setText("");
            textfield_3.setText("");
            textfield_4.setText("");
        }
        if (e.getSource()==ansButton){
            textfield_1.setText("    Returns last result.");
            textfield_2.setText("");
            textfield_3.setText("");
            textfield_4.setText("");
        }
    }
}