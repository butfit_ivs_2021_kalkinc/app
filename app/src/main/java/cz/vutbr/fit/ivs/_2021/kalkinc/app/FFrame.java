/*
 * Copyright (c) 2021. Dítě Jindřich, Lebedenko Anastasiia, Čuhanič Jakub,
 *  Hradil Michal
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package cz.vutbr.fit.ivs._2021.kalkinc.app;
import cz.vutbr.fit.ivs._2021.kalkinc.lib.*;
import cz.vutbr.fit.ivs._2021.kalkinc.lib.parser.ParserException;

import javax.swing.*;

import javax.swing.border.LineBorder;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

public class FFrame extends JFrame implements ActionListener, KeyListener {
    JFrame frame;
    JTextField textfield;
    List<Token> tokenList = new ArrayList<>();
    CalculatorImpl calculator = new CalculatorImpl();
    String numbers = "";
    String par_l = "(";
    String par_r = ")";
    String min = "-";
    String temp = "";
    String mew = "";
    String mew_1 = "";

    //buttons' arrays that go into panels
    JButton[] numberButtons = new JButton[10];
    JButton[] functionButtons = new JButton[15];
    JButton[] plusButtons = new JButton[4];
    JPanel plus;
    JPanel number;
    JPanel function;
    JPanel background;

    //initializing buttons
    JButton addButton,subButton,mulButton,divButton, decButton;
    JButton delButton, negButton, logButton, powButton, rootButton;
    JButton equButton, clrButton, facButton;
    JButton par_leftButton, par_rightButton, ansButton, mButton, m_plusButton, m_minusButton;

    //font setting
    Font myFont = new Font("Roboto",Font.BOLD,20);

    //toolbar
    JMenuBar menuBar;
    JMenu helpMenu;

    FFrame(){

        //frame settings
        frame = new JFrame("Calculator");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(615, 650);
        frame.getContentPane().setBackground(new Color (255, 255,255));
        frame.setLayout(null);
        frame.setResizable(false);
        frame.setVisible(true);
        frame.addKeyListener(this);

        //icon - continue
        ImageIcon image = new ImageIcon("app/src/image/Icon.png");
        frame.setIconImage(image.getImage());

        //screen with result
        textfield = new JTextField();
        textfield.setBounds(39, 39, 525, 75);
        textfield.setFont(myFont);
        textfield.setEditable(false);
        textfield.setBorder(new LineBorder((new Color(255, 255, 255)),10, false));
        textfield.setBackground(Color.WHITE);
        textfield.setVisible(true);

        //label behind textfield
        background = new JPanel();
        background.setBackground(new Color(183, 255, 203));
        background.setBounds(26, 26, 550, 100);


        //buttons' text
        addButton = new JButton("+");
        subButton = new JButton("-");
        mulButton = new JButton("\u00D7");
        divButton = new JButton("\u00F7");
        decButton = new JButton(".");
        equButton = new JButton("=");
        delButton = new JButton("DEL");
        clrButton = new JButton("CE");
        negButton = new JButton("\u00B1");
        logButton = new JButton("log");
        powButton = new JButton("x\u00B2");
        rootButton = new JButton("\u221A");
        facButton = new JButton("!");
        par_leftButton = new JButton("(");
        par_rightButton = new JButton(")");
        ansButton = new JButton ("Ans");
        mButton = new JButton ("M");
        m_plusButton = new JButton ("M+");
        m_minusButton = new JButton ("M-");

        //initializing arrays, setting action listeners, focusability
        plusButtons[0] = addButton;
        plusButtons[1] = subButton;
        plusButtons[2] = mulButton;
        plusButtons[3] = divButton;

        functionButtons[0] = decButton;
        functionButtons[1] = equButton;
        functionButtons[2] = delButton;
        functionButtons[3] = negButton;
        functionButtons[4] = logButton;
        functionButtons[5] = powButton;
        functionButtons[6] = rootButton;
        functionButtons[7] = facButton;
        functionButtons[8] = ansButton;
        functionButtons[9] = clrButton;
        functionButtons[10] = par_leftButton;
        functionButtons[11] = par_rightButton;
        functionButtons[12] = mButton;
        functionButtons[13] = m_minusButton;
        functionButtons[14] = m_plusButton;

        for(int i =0;i<4;i++) {
            plusButtons[i].addActionListener(this);
            plusButtons[i].setFont(myFont);
            plusButtons[i].setFocusable(false);
            plusButtons[i].setBackground(new Color(183,255,203));
            plusButtons[i].setBorder(new LineBorder(new Color(183,255,203)));
        }

        for(int i =0;i<15;i++) {
            functionButtons[i].addActionListener(this);
            functionButtons[i].setFont(myFont);
            functionButtons[i].setFocusable(false);
            functionButtons[i].setBackground(new Color(183,255,203));
            functionButtons[i].setBorder(new LineBorder(new Color(183,255,203)));
        }
        functionButtons[4].setMnemonic('L');
        functionButtons[5].setMnemonic('P');
        functionButtons[6].setMnemonic('R');
        functionButtons[7].setMnemonic('F');
        functionButtons[8].setMnemonic('A');
        functionButtons[10].setMnemonic(')');
        functionButtons[11].setMnemonic('(');
        functionButtons[12].setMnemonic('M');
        for(int i =0;i<10;i++) {
            numberButtons[i] = new JButton(String.valueOf(i));
            numberButtons[i].addActionListener(this);
            numberButtons[i].setFont(myFont);
            numberButtons[i].setBackground(new Color(242,251,245));
            numberButtons[i].setBorder(new LineBorder(new Color(242,251,245)));
            numberButtons[i].setFocusable(false);
        }

        //change settings of decimal button
        decButton.setBackground(new Color(242,251,245));
        decButton.setBorder(new LineBorder(new Color (242, 251, 245)));

        //position and size of decimal, zero and equals buttons
        //(they don't go into panels, because they aren't proportional)
        decButton.setBounds(220,490, 75, 75);
        decButton.setVisible(true);
        numberButtons[0].setBounds(305, 490, 75, 75);
        numberButtons[0].setVisible(true);
        equButton.setBounds(390,490,160,75);
        equButton.setVisible(true);

        //panels' settings (what buttons go in there, size, position)
        plus = new JPanel();
        plus.setBounds(475, 150, 75, 330);
        plus.setLayout(new GridLayout(4,1,10,10));
        plus.setBackground(new Color(255,255,255));

        plus.add(divButton);
        plus.add(mulButton);
        plus.add(subButton);
        plus.add(addButton);

        number = new JPanel();
        number.setBounds(220, 150, 245, 330);
        number.setLayout(new GridLayout(4,3,10,10));
        number.setBackground(new Color(255,255,255));

        number.add(delButton);
        number.add(par_leftButton);
        number.add(par_rightButton);
        number.add(numberButtons[7]);
        number.add(numberButtons[8]);
        number.add(numberButtons[9]);
        number.add(numberButtons[4]);
        number.add(numberButtons[5]);
        number.add(numberButtons[6]);
        number.add(numberButtons[1]);
        number.add(numberButtons[2]);
        number.add(numberButtons[3]);
        number.add(decButton);

        function = new JPanel();
        function.setBounds(50, 150, 160, 415);
        function.setLayout(new GridLayout(5,2,10,10));
        function.setBackground(new Color (255,255,255));

        function.add(ansButton);
        function.add(clrButton);
        function.add(mButton);
        function.add(powButton);
        function.add(m_plusButton);
        function.add(facButton);
        function.add(m_minusButton);
        function.add(rootButton);
        function.add(logButton);
        function.add(negButton);

        //adding all the panels and separate buttons into a frame
        frame.add(plus);
        frame.add(number);
        frame.add(function);
        frame.add(background);
        frame.add(textfield);
        frame.add(decButton);
        frame.add(numberButtons[0]);
        frame.add(equButton);
        frame.setVisible(true);

        //add help option to menu
        menuBar = new JMenuBar();
        helpMenu = new JMenu("Help");
        menuBar.add(Box.createHorizontalGlue());
        menuBar.add(helpMenu);
        frame.setJMenuBar(menuBar);
        helpMenu.addMenuListener(new MenuListener() {
            @Override
            public void menuSelected(MenuEvent e) {
                    new HelpWindow();
            }

            @Override
            public void menuDeselected(MenuEvent e) {

            }

            @Override
            public void menuCanceled(MenuEvent e) {

            }
        });
        helpMenu.setMnemonic(KeyEvent.VK_H); // Alt + h for help
        menuBar.setBackground(new Color(255,255,255));
    }

    public static void main(String[] args) {

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        //buttons implemented in lib
        if(e.getSource()==ansButton) {
            textfield.setHorizontalAlignment(SwingConstants.LEFT);
            tokenList.add(new TokenImpl("Ans"));
            textfield.setText(textfield.getText().concat(String.valueOf(calculator.getResult())));
            if (!numbers.equals("")) {
                tokenList.add(new TokenImpl(numbers));
                numbers = "";
            }
        }
        if(e.getSource()==mButton) {
            textfield.setHorizontalAlignment(SwingConstants.LEFT);
            tokenList.add(new TokenImpl("M"));
            textfield.setText("M");
        }
        if(e.getSource()==m_plusButton) {
            tokenList.add(new TokenImpl("M+"));
            textfield.setText("M+");
        }
        if(e.getSource()==m_minusButton) {
            tokenList.add(new TokenImpl("M-"));
            textfield.setText("M-");
        }
        if(e.getSource()==logButton) {
            textfield.setText(textfield.getText().concat("log"));
            if (!numbers.equals("")) {
                tokenList.add(new TokenImpl(numbers));
                numbers = "";
            }
            tokenList.add(new TokenImpl("log"));
        }
        if(e.getSource()==powButton) {
            textfield.setText(textfield.getText().concat("^"));
            if (!numbers.equals("")) {
                tokenList.add(new TokenImpl(numbers));
                numbers = "";
            }
            tokenList.add(new TokenImpl("^"));
        }
        if(e.getSource()==facButton) {
            textfield.setText(textfield.getText().concat("!"));
            if (!numbers.equals("")) {
                tokenList.add(new TokenImpl(numbers));
                numbers = "";
            }
            tokenList.add(new TokenImpl("!"));
        }

        if(e.getSource()==rootButton) {
            textfield.setText(textfield.getText().concat("\u221A"));

            if (!numbers.equals("")) {
                tokenList.add(new TokenImpl(numbers));
                numbers = "";
            }
            tokenList.add(new TokenImpl("root"));
        }
        if(e.getSource()==par_leftButton) {
            textfield.setText(textfield.getText().concat("("));
            if (!numbers.equals("")) {
                tokenList.add(new TokenImpl(numbers));
                numbers = "";
            }
            tokenList.add(new TokenImpl("("));
        }
        if(e.getSource()==par_rightButton) {
            textfield.setText(textfield.getText().concat(")"));
            if (!numbers.equals("")) {
                tokenList.add(new TokenImpl(numbers));
                numbers = "";
            }
            tokenList.add(new TokenImpl(")"));
        }
        if(e.getSource()==addButton) {
            textfield.setText(textfield.getText().concat("+"));
            if (!numbers.equals("")) {
                tokenList.add(new TokenImpl(numbers));
                numbers = "";
            }
            tokenList.add(new TokenImpl("+"));
        }
        if(e.getSource()==subButton) {
            textfield.setText(textfield.getText().concat("-"));
            if (!numbers.equals("")) {
                tokenList.add(new TokenImpl(numbers));
                numbers = "";
            }
            if (tokenList.size() == 0 || tokenList.get(tokenList.size()-1).getValue().equals("(")) {
                numbers = numbers.concat("-");
            } else {
                tokenList.add(new TokenImpl("-"));
            }
        }
        if(e.getSource()==divButton) {
            textfield.setText(textfield.getText().concat("/"));
            if (!numbers.equals("")) {
                tokenList.add(new TokenImpl(numbers));
                numbers = "";
            }
            tokenList.add(new TokenImpl("/"));
        }

        // doesn't work 8 * -9 = 72
        // works        -8 * 9 = -72
        if(e.getSource()==mulButton) {
            textfield.setText(textfield.getText().concat("*"));
            if (!numbers.equals("")) {
                tokenList.add(new TokenImpl(numbers));
                numbers = "";
            }
            tokenList.add(new TokenImpl("*"));
        }
        if(e.getSource()==equButton) {
            if (!numbers.equals("")) {
                tokenList.add(new TokenImpl(numbers));
                numbers = "";
            }
            try {
                calculator.setExpression(tokenList);
                textfield.setHorizontalAlignment(SwingConstants.RIGHT);
                textfield.setText(String.valueOf(calculator.getResult()));
                numbers = String.valueOf(calculator.getResult());
                tokenList.clear();
            } catch (ParserException | IllegalArgumentException exception) {
                textfield.setText(String.valueOf(exception.getMessage()));
            }
        }
        for(int i=0;i<10;i++) {
            if(e.getSource() == numberButtons[i]) {
                textfield.setHorizontalAlignment(SwingConstants.LEFT);
                textfield.setText(textfield.getText().concat(String.valueOf(i)));
                numbers = numbers.concat(String.valueOf(i));
            }
        }
        //buttons implemented only in app (not lib)
        if(e.getSource()==clrButton) {
            textfield.setText("");
            tokenList.clear();
            numbers = "";
        }
        if(e.getSource()==delButton) {
            String string = textfield.getText();
            textfield.setText("");
            if (!string.equals("")) {
                if (string.length() == 1) {
                    textfield.setText("");
                    tokenList.clear();
                    numbers = "";
                } else {
                    for (int i = 0; i < string.length() - 1; i++) {
                        textfield.setText(textfield.getText() + string.charAt(i));
                    }
                    if (!numbers.equals("")) {
                        numbers = numbers.substring(0, numbers.length() -1);
                    } else {
                        int lastElement = tokenList.size() - 1;
                        tokenList.remove(lastElement);
                    }
                }
            }
        }
        if(e.getSource()==decButton) {
            if (!numbers.equals("")) {
                numbers = numbers.concat(".");
                textfield.setText(textfield.getText().concat("."));
            } else {
                textfield.setHorizontalAlignment(SwingConstants.LEFT);
                textfield.setText(textfield.getText().concat("0."));
                numbers="0.";
                for(int i=0;i<10;i++) {
                    if(e.getSource() == numberButtons[i]) {
                        textfield.setHorizontalAlignment(SwingConstants.LEFT);
                        textfield.setText(textfield.getText().concat(String.valueOf(i)));
                        numbers = numbers.concat(String.valueOf(i));
                    }
                }
            }
        }
        if(e.getSource()==negButton) {
            if (tokenList.size()==0){
                numbers = min.concat(numbers);
                textfield.setText((par_l.concat(numbers).concat(par_r)));
            } else {
                temp = numbers;
                temp = min.concat(temp);
                mew = textfield.getText();
                mew_1 = mew.replace(String.valueOf(numbers), "");
                numbers = min.concat(numbers);
                textfield.setText(mew_1.concat(par_l.concat(temp).concat(par_r)));
            }
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
        //buttons implemented in app and lib
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == 48 || e.getKeyCode() == 96) {
            textfield.setHorizontalAlignment(SwingConstants.LEFT);
            textfield.setText(textfield.getText().concat(String.valueOf(0)));
            numbers = numbers.concat(String.valueOf(0));
        }
        if (e.getKeyCode() == 49 || e.getKeyCode() == 97) {
            textfield.setHorizontalAlignment(SwingConstants.LEFT);
            textfield.setText(textfield.getText().concat(String.valueOf(1)));
            numbers = numbers.concat(String.valueOf(1));
        }
        if (e.getKeyCode() == 50 || e.getKeyCode() == 98) {
            textfield.setHorizontalAlignment(SwingConstants.LEFT);
            textfield.setText(textfield.getText().concat(String.valueOf(2)));
            numbers = numbers.concat(String.valueOf(2));
        }
        if (e.getKeyCode() == 51 || e.getKeyCode() == 99) {
            textfield.setHorizontalAlignment(SwingConstants.LEFT);
            textfield.setText(textfield.getText().concat(String.valueOf(3)));
            numbers = numbers.concat(String.valueOf(3));
        }
        if (e.getKeyCode() == 52 || e.getKeyCode() == 100) {
            textfield.setHorizontalAlignment(SwingConstants.LEFT);
            textfield.setText(textfield.getText().concat(String.valueOf(4)));
            numbers = numbers.concat(String.valueOf(4));
        }
        if (e.getKeyCode() == 53 || e.getKeyCode() == 101) {
            textfield.setHorizontalAlignment(SwingConstants.LEFT);
            textfield.setText(textfield.getText().concat(String.valueOf(5)));
            numbers = numbers.concat(String.valueOf(5));
        }
        if (e.getKeyCode() == 54 || e.getKeyCode() == 102) {
            textfield.setHorizontalAlignment(SwingConstants.LEFT);
            textfield.setText(textfield.getText().concat(String.valueOf(6)));
            numbers = numbers.concat(String.valueOf(6));
        }
        if (e.getKeyCode() == 55 || e.getKeyCode() == 103) {
            textfield.setHorizontalAlignment(SwingConstants.LEFT);
            textfield.setText(textfield.getText().concat(String.valueOf(7)));
            numbers = numbers.concat(String.valueOf(7));
        }
        if (e.getKeyCode() == 56 || e.getKeyCode() == 104) {
            textfield.setHorizontalAlignment(SwingConstants.LEFT);
            textfield.setText(textfield.getText().concat(String.valueOf(8)));
            numbers = numbers.concat(String.valueOf(8));
        }
        if ((e.getKeyCode() == 57 || e.getKeyCode() == 105) && e.getKeyCode()!=16) {
            textfield.setHorizontalAlignment(SwingConstants.LEFT);
            textfield.setText(textfield.getText().concat(String.valueOf(9)));
            numbers = numbers.concat(String.valueOf(9));
            }
        if (e.getKeyCode() == 521 || e.getKeyCode() == 107) {
            textfield.setText(textfield.getText().concat("+"));
            if (!numbers.equals("")) {
                tokenList.add(new TokenImpl(numbers));
                numbers = "";
            }
            tokenList.add(new TokenImpl("+"));
        }
        if (e.getKeyCode() == 45 || e.getKeyCode() == 109) {
            textfield.setText(textfield.getText().concat("-"));
            if (!numbers.equals("")) {
                tokenList.add(new TokenImpl(numbers));
                numbers = "";
            }
            if (tokenList.size() == 0 || tokenList.get(tokenList.size()-1).getValue().equals("(")) {
                numbers = numbers.concat("-");
            } else {
                tokenList.add(new TokenImpl("-"));
            }
        }
        if (e.getKeyCode() == 106) { // works only on numpad
            textfield.setText(textfield.getText().concat("*"));
            if (!numbers.equals("")) {
                tokenList.add(new TokenImpl(numbers));
                numbers = "";
            }
            tokenList.add(new TokenImpl("*"));
        }
        if (e.getKeyCode() == 47 || e.getKeyCode() == 111) {
            textfield.setText(textfield.getText().concat("/"));
            if (!numbers.equals("")) {
                tokenList.add(new TokenImpl(numbers));
                numbers = "";
            }
            tokenList.add(new TokenImpl("/"));
        }
        if(e.getKeyCode() == 517) { // Factorial not working
            textfield.setText(textfield.getText().concat("!"));
            if (!numbers.equals("")) {
                tokenList.add(new TokenImpl(numbers));
                numbers = "";
            }
            tokenList.add(new TokenImpl("!"));
        }
        if(e.getKeyCode() == 8) {
            String string = textfield.getText();
            textfield.setText("");
            if (!string.equals("")) {
                if (string.length() == 1) {
                    textfield.setText("");
                    tokenList.clear();
                    numbers = "";
                } else {
                    for (int i = 0; i < string.length() - 1; i++) {
                        textfield.setText(textfield.getText() + string.charAt(i));
                    }
                    if (!numbers.equals("")) {
                        numbers = numbers.substring(0, numbers.length() -1);
                    } else {
                        int lastElement = tokenList.size() - 1;
                        tokenList.remove(lastElement);
                    }
                }
            }
        }
        if(e.getKeyCode() == 46 || e.getKeyCode() == 110) {
            if (!numbers.equals("")) {
                numbers = numbers.concat(".");
                textfield.setText(textfield.getText().concat("."));
            } else {
                textfield.setHorizontalAlignment(SwingConstants.LEFT);
                textfield.setText(textfield.getText().concat("0."));
                numbers="0.";
                for(int i=0;i<10;i++) {
                    if(e.getSource() == numberButtons[i]) {
                        textfield.setHorizontalAlignment(SwingConstants.LEFT);
                        textfield.setText(textfield.getText().concat(String.valueOf(i)));
                        numbers = numbers.concat(String.valueOf(i));
                    }
                }
            }
        }
        if (e.getKeyCode() == 10) {
            if (!numbers.equals("")) {
                tokenList.add(new TokenImpl(numbers));
                numbers = "";
            }
            try {
                calculator.setExpression(tokenList);
                textfield.setHorizontalAlignment(SwingConstants.RIGHT);
                textfield.setText(String.valueOf(calculator.getResult()));
                numbers = String.valueOf(calculator.getResult());
                tokenList.clear();
            } catch (ParserException | IllegalArgumentException exception) {
                textfield.setText(String.valueOf(exception.getMessage()));
            }
        }
        if(e.getKeyCode() == 127){
            textfield.setText("");
            tokenList.clear();
            numbers = "";
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}
